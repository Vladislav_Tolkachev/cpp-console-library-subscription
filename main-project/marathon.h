#ifndef MARATHON_H
#define MARATHON_H

#include "constants.h"

struct time_s
{
    int hourse;
    int minutes;
    int seconds;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct marathon
{
    int number_p;
    person member;
    time_s start;
    time_s finish;
    char club[MAX_STRING_SIZE];
};

#endif 
