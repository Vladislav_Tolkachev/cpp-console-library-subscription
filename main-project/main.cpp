#include <iostream>

using namespace std;

#include "marathon.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(marathon* m_result)
{
    /********** ����� ���������� **********/
    cout << "����� ���������: ";
    // ����� ������ ���������
    cout << m_result->number_p << ' ';
    // ����� ���
    cout << "���: ";
    cout << m_result->member.last_name << ' ';
    cout << m_result->member.first_name[0] << '.';
    cout << m_result->member.middle_name[0] << '.' << ' ';
    // ����� �������
    cout << "������ ��������: ";
    cout << m_result->start.hourse << ':';
    cout << m_result->start.minutes << ':';
    cout << m_result->start.seconds << ' ';
    // ����� �������
    cout << "����� ��������� ������: ";
    cout << m_result->finish.hourse << ':';
    cout << m_result->finish.minutes << ':';
    cout << m_result->finish.seconds << ' ';
    // ����� �����
    cout << "����: ";
    cout << m_result->club << ' ';
    cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �1. ���������� ��������\n";
    cout << "�����: �������� ���������\n\n";
    cout << "������: 14�\n\n";
    marathon* m_result[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", m_result, size);
        cout << "***** ���������� �������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(m_result[i]);
        }
        
        bool (*check_function)(marathon*) = NULL;

		cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
		cout << "1) ��������� � �� ���������� �� ����� �������� \n";
		cout << "2) ��������� � ������� ��������� ����� ��� 2:50:00\n";
		cout << "\n������� ����� ���������� ������: ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_marathon_by_club; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ��������� � �� ���������� �� ����� �������� *****\n\n";
			break;
		case 2:
			check_function = check_marathon_by_best_time; // ����������� � ��������� �� ������� ��������������� �������
			cout << "***** ��������� � ������� ��������� ����� ��� 2:50:00 *****\n\n";
			break;
	
		default:
			throw "������������ ����� ������";
		}
		if (check_function)
		{
			int new_size;
			marathon** filtered = filter(m_result, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
        for (int i = 0; i < size; i++)
        {
            delete m_result[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}