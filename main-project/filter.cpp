#include "filter.h"
#include <cstring>
#include <iostream>

marathon** filter(marathon* array[], int size, bool (*check)(marathon* element), int& result_size)
{
	marathon** result = new marathon * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_marathon_by_club(marathon* element)
{
	return strcmp(element->club, "�������") == 0;
}

bool check_marathon_by_best_time(marathon* element)
{
	return element->finish.hourse * 60 + element->finish.minutes < 170;
}
