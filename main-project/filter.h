#ifndef FILTER_H
#define FILTER_H

#include "marathon.h"

marathon** filter(marathon* array[], int size, bool (*check)(marathon* element), int& result_size);

bool check_marathon_by_club(marathon* element);
bool check_marathon_by_best_time(marathon* element);

#endif